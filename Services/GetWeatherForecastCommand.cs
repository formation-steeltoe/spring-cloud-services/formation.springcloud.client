using System.Collections.Generic;
using System.Threading.Tasks;
using Formation.SpringCloud.Client.Models;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Steeltoe.CircuitBreaker.Hystrix;

namespace Formation.SpringCloud.Client.Services
{
    public class GetWeatherForecastCommand : HystrixCommand<List<WeatherForecastModel>>
    {
        private const string CACHE_KEY = "GetWeatherForecastCommand::CacheKey";
        private readonly ILogger<GetWeatherForecastCommand> _logger;
        private readonly IWeatherForecastService _service;
        private readonly IMemoryCache _cache;

        public GetWeatherForecastCommand(IHystrixCommandOptions options,
                                         ILogger<GetWeatherForecastCommand> logger,
                                         IWeatherForecastService service,
                                         IMemoryCache cache) : base(options)
        {
            _logger = logger;
            _service = service;
            _cache = cache;
        }

        public async Task<List<WeatherForecastModel>> GetWeatherForecast()
        {
            return await ExecuteAsync();
        }

        protected override async Task<List<WeatherForecastModel>> RunAsync()
        {
            var result = await _service.GetWeatherForecast();
            _cache.Set(CACHE_KEY, result);
            _logger.LogInformation("Run: {0}", result);
            return result;
        }

        protected override async Task<List<WeatherForecastModel>> RunFallbackAsync()
        {
            _logger.LogInformation("RunFallback");

            List<WeatherForecastModel> result;

            if (! _cache.TryGetValue(CACHE_KEY, out result))
            {
                result = new List<WeatherForecastModel>()
                {
                    new WeatherForecastModel()
                    {
                        summary = "Service non-disponible pour le moment. Veuillez réesayer plus tard."
                    }
                };
            }

            return await Task.FromResult(result);
        }
    }
}
