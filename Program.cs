using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Steeltoe.Common.Hosting;
using Steeltoe.Extensions.Configuration.CloudFoundry;
using Steeltoe.Extensions.Logging.DynamicLogger;

namespace Formation.SpringCloud.Client
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                })
                
                // Écoute sur le port défini par la variable d'environnement 'PORT' ou 'SERVER_PORT'
                // Sinon, écoute sur le port 8080 par défaut.
                .UseCloudHosting(8080)

                // Add VCAP_* configuration data
                .AddCloudFoundry()

                // Ajouter le Steeltoe Dynamic Logging provider
                .AddDynamicLogging();
    }
}
