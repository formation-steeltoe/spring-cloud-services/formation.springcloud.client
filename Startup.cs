using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Formation.SpringCloud.Client.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Steeltoe.CircuitBreaker.Hystrix;
using Steeltoe.Discovery.Client;
using Steeltoe.Management.CloudFoundry;
using Steeltoe.Management.Endpoint.Refresh;

namespace Formation.SpringCloud.Client
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();

            // Ajouter le WeatherForecastService pour la communication avec le service baskend
            services.AddTransient<IWeatherForecastService, WeatherForecastService>();

            // Command Hystrix qui utilise le WeatherForecastService
            services.AddHystrixCommand<GetWeatherForecastCommand>("WeatherForecastService", Configuration);

            // Ajouter MemoryCache pour le comportement de fallback des commandes Hystrix
            services.AddMemoryCache();

            // Add management endpoint services like this
            services.AddCloudFoundryActuators(Configuration);
            services.AddRefreshActuator(Configuration);

            // Add the Steeltoe Discovery Client service
            services.AddDiscoveryClient(Configuration);

            // Ajouter les metriques en continue Hystrix pour permettre le monitoring.
            services.AddHystrixMetricsStream(Configuration);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });

            // Add management endpoints into pipeline like this
            app.UseCloudFoundryActuators();
            app.UseRefreshActuator();

            // Use the Steeltoe Discovery Client service
            app.UseDiscoveryClient();

            // Add Hystrix Metrics context to pipeline
            app.UseHystrixRequestContext();

            // Start Hystrix metrics stream service
            app.UseHystrixMetricsStream();
        }
    }
}
